﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Flatten_Images.Models
{
    public class FlattenImageResponse
    {
        public int NumberOfImages { get; set; }
        public string ImageBase64 { get; set; }
    }
}
