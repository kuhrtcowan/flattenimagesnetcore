﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Flatten_Images.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Flatten_Images.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly ILogger<ImagesController> _logger;

        public ImagesController(ILogger<ImagesController> logger)
        {
            _logger = logger;
        }

        [HttpGet("ImageSharp")]
        public async Task<FlattenImageResponse> MergeWithImageSharp()
        {
            var background = await getImageFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-base3.png");
            var cabinets = await getImageFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-cabinets3.png");
            var floors = await getImageFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-floor2.png");
            var finalImage = new Image<Rgba32>(1000, 675);
            var imageBase64 = "";

            finalImage.Mutate(o => o
                .DrawImage(background, new SixLabors.ImageSharp.Point(0, 0), 1f)
                .DrawImage(cabinets, new SixLabors.ImageSharp.Point(0, 0), 1f)
                .DrawImage(floors, new SixLabors.ImageSharp.Point(0, 0), 1f)
            );

            finalImage.Save("Images/image-sharp.png");

            using (var ms = new MemoryStream())
            {
                finalImage.SaveAsPng(ms);
                finalImage.Dispose();
                var imageBytes = ms.ToArray();
                imageBase64 = Convert.ToBase64String(imageBytes);
                imageBytes = null;
                ms.Dispose();
            }

            return new FlattenImageResponse
            {
                NumberOfImages = 3,
                ImageBase64 = imageBase64,
            };
        }

        [HttpGet("Bitmap")]
        public async Task<FlattenImageResponse> MergeWithBitmap()
        {
            var background = await getBitmapFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-base3.png");
            var cabinets = await getBitmapFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-cabinets3.png");
            var floors = await getBitmapFromUrl("https://188-010-bhvisualizerpoc-dev.azurewebsites.net/images/renderings/kitchen/kitchen_test-floor2.png");
            var result = new Bitmap(background.Width, background.Height);
            var finalImage = Graphics.FromImage(result);
            var imageBase64 = "Did it and was too lazy to make it a base64 string!";

            finalImage.DrawImageUnscaled(background, 0, 0);
            finalImage.DrawImageUnscaled(cabinets, 0, 0);
            finalImage.DrawImageUnscaled(floors, 0, 0);

            result.Save("Images/bitmap.png");

            return new FlattenImageResponse
            {
                NumberOfImages = 3,
                ImageBase64 = imageBase64,
            };
        }

        private async Task<SixLabors.ImageSharp.Image> getImageFromUrl(string url)
        {
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.GetAsync(url);
            Stream inputStream = await response.Content.ReadAsStreamAsync();
            SixLabors.ImageSharp.Image image = SixLabors.ImageSharp.Image.Load<Rgba32>(inputStream);

            return image;
        }

        private async Task<Bitmap> getBitmapFromUrl(string url)
        {
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.GetAsync(url);
            Stream inputStream = await response.Content.ReadAsStreamAsync();
            Bitmap image = new Bitmap(inputStream);

            return image;
        }
    }
}